---
author:
- address: School of Computing, University of Leeds, UK
  email: T.Ranner\@leeds.ac.uk
  name: Tom Ranner
autoPlayMedia: true
backgroundTransition: none
bibliography: ./bib/library.bibtex
center: false
csl: ./bib/ima.csl
css: ./css/metropolis.css
include-before: |
  ::: {style="display:none"}
  $$
    \renewcommand{\vec}[1]{\boldsymbol{#1}}
  $$
  :::
link-citations: true
logos:
- alt: University of Leeds
  source: ./img/Leeds_Logo.svg
mathjaxurl: ./js/mathjax/es5/tex-chtml-full.js
reference-section-title: References
slides: "`<a href=\"https://tom-ranner.gitlab.io/fsi-talk-2022\">`{=html}https://tom-ranner.gitlab.io/fsi-talk-2022`</a>`{=html}"
title: FSI Research talk - some problems in biology
transition: none
---

# Why fluid mechanics is important in biology?

## Locomotion

::: container
::: col
![](video/sperm.webm){width="100%"}

`<small>`{=html} [youtu.be/4Vzi9VmnWFQ](https://youtu.be/4Vzi9VmnWFQ) `</small>`{=html}
:::

::: col
![](video/birds.mp4){width="100%"}

`<small>`{=html} [youtu.be/0-XsgYJyEyc](https://youtu.be/0-XsgYJyEyc) `</small>`{=html}
:::
:::

Biological organisms move through *fluid* environments.\
The way animals move is a key behavioural indicator.

## Other fluids in medicine and biology

::: container
::: col
![](video/stent.mp4){width="90%"}

`<small>`{=html} [youtu.be/I12PMiX5h3E](https://youtu.be/I12PMiX5h3E) `</small>`{=html}
:::

::: col
![](video/coughs.mp4){width="90%"}

`<small>`{=html} [youtu.be/9qqHOKUXU5U](https://youtu.be/9qqHOKUXY5U) `</small>`{=html}
:::
:::

## Dead fish swim

![](video/dead-fish.mp4){.stretch}

`<small>`{=html}[@BEAL2006]`</small>`{=html}

## Challenges in biological problems

![](img/biology-challenges.png){.stretch}

`<small>`{=html}[Image source](https://www.forbes.com/sites/alexandrasternlicht/2020/07/31/human-sperm-dont-wiggle-they-roll-like-playful-otters-says-new-research/?sh=25e155444ec4)`</small>`{=html}

# Life at low Reynolds number

## What is Reynolds number?

$$
\mathrm{Re} = \frac{\rho L U}{\eta}
$$ where

-   $\rho$ is density of fluid
-   $L$ is a length scale (body size)
-   $U$ is speed (swimming speed)
-   $\eta$ is viscocity of fluid

## Low Reynolds number flows

![](video/reversible.mp4){width="75%"}

Flows are reversible

`<small>`{=html}[Lecture by GI Taylor: Low Reynolds number flows](https://www.youtube.com/watch?v=51-6QCJTAjU)`</small>`{=html}

## Scallop theorem

![](video/purcell-swimmer.mp4){width="60%"}

| A swimmer that exhibits time-symmetric motion cannot achieve net displacement in a low Reynolds number Newtonian fluid environment.

`<small>`{=html}[@Pur77]`</small>`{=html}

## Some examples of solutions

::: container
::: col
Chlamydomonas' breaststroke

![](video/chlamy.mp4){width="100%"}

`<small>`{=html} [youtu.be/9dwWIEGjdAo](https://youtu.be/9dwWIEGjdAo) `</small>`{=html}
:::

::: col
Sperm cells, flagellar beating, and travelling waves

![](video/undulation.mp4){width="100%"}

`<small>`{=html} [youtu.be/GgZHziFWR7M](https://youtu.be/GgZHziFWR7M) `</small>`{=html}
:::
:::

# Some open questions

## Integrated modelling

**Modelling fluid environment, body and neural control together**

-   How do animals generate efficient locomotion patterns? [@Denham2018]

-   How robust or resilient is locomotion strategy or strategies?

## Active matter modelling

**Animals can actively control their material parameters**

-   How do they do this? [@Hamlet2018]

-   Why do they do this?

## Locomotion in complex fluids

**Most animals move in complex fluids**

-   e.g. sperm in mucus, *C. elegans* in rotting vegetation

-   We have some models of complex fluids. How to integrate with swimmer? [@Schoeller2021]

-   Slender body theory [@Hewitt2018], reduced rod models [@Ranner2020], direct numerical simulation, ...

## Animal interactions

**Animals rarely live alone**

-   How does fluid dynamics effect animals trying to move relative to one another? (e.g. mating)

-   How does having another swimmers nearby effect swimmer properties? [@Schoeller2020]

## Good computational methods

**How can we perform *accurate*, *efficient* and *robust* numerical simulations of active processes?**

::: container
::: col
-   My main focus / background

-   Many topics you will learn in this course will help

-   I am developing fundamental approaches based on arbitrary Lagrangian-Eulerian formulations, moving meshes and (isoparametric) finite element methods [@Elliott2020]
:::

::: col
![](./video/mesh-small.ogv)
:::
:::

## Useful references

-   Animal locomotion: [@Gra68; @Lauga2009; @Cicconofri2019; @Cohen2010]
-   Fluid dynamics at low Reynolds number \[@Childress1981;@Lighthill1975;@Lig76;Brennen1977;@Pur77;@Yates1986;@Fauci2006\]
-   Recent review [arxiv:1912.06710](https://arxiv.org/abs/1912.06710)

# What I am interested in

## About me

-   PhD, Mathematics. "Computational surface PDE".

-   Lecturer in School of Computing since 2017.

-   Now considered myself *applied mathematician working as part of a larger interdisciplinary team*.

-   **Analysis based computational modelling**

## Research interests

-   Background in numerical analysis of PDE, especially free boundary problems and related areas

-   General interests in flow in complex and moving domains (multiphase flow, flow on surfaces)

-   Recently interested in biological swimmers as an application of methods and also source of problems and requirements

-   *Looking for PhD students* [Project: Geometric numerical integration –in geophysical and environmental fluid dynamics](	https://fluid-dynamics.leeds.ac.uk/projects/geometric-numerical-integration-in-geophysical-and-environmental-fluid-dynamics/)

## Thank you for your attention

I'm happy to discuss any areas around biological fluids or computational methods and modelling
